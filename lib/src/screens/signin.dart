import 'dart:convert';
import 'package:daangor/config/ui_icons.dart';
import 'package:daangor/provider/postlogin.dart';
import 'package:daangor/provider/userdetail.dart';
import 'package:daangor/src/screens/home.dart';
import 'package:daangor/src/widgets/SocialMediaWidget.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class SignInWidget extends StatefulWidget {
   final Future<PostLogin> post;
   SignInWidget(
    {
    
    Key key, 
    this.post
  
    }
    
    ) : super(key : key);
  @override
  _SignInWidgetState createState() => _SignInWidgetState();
}

class _SignInWidgetState extends State<SignInWidget> {
 
  Future<Userdetail> _futureAlbum;
  Response response;
  Dio dio;

  bool _showPassword = false;
  TextEditingController emailcontroller = TextEditingController();
 TextEditingController passwordcontroller = TextEditingController();
TextEditingController usernamecontroller = TextEditingController();

 String emails;
  String passwords;
  
  
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).accentColor,
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Stack(
              children: <Widget>[
                Container(
                  width: double.infinity,
                  padding: EdgeInsets.symmetric(vertical: 30, horizontal: 30),
                  margin: EdgeInsets.symmetric(vertical: 65, horizontal: 20),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      color: Theme.of(context).primaryColor,
                      boxShadow: [
                        BoxShadow(
                            color: Theme.of(context).hintColor.withOpacity(0.2), offset: Offset(0, 10), blurRadius: 20)
                      ]),
                  child: Column(
                    children: <Widget>[
                      SizedBox(height: 25),
                      Text('Sign In', style: Theme.of(context).textTheme.display2),
                      SizedBox(height: 20),
                      new TextFormField(
                        style: TextStyle(color: Theme.of(context).focusColor),
                        keyboardType: TextInputType.emailAddress,
                        decoration: new InputDecoration(
                          hintText: 'Email Address',
                          hintStyle: Theme.of(context).textTheme.body1.merge(
                                TextStyle(color: Theme.of(context).focusColor.withOpacity(0.6)),
                              ),
                          enabledBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Theme.of(context).focusColor.withOpacity(0.2))),
                          focusedBorder:
                              UnderlineInputBorder(borderSide: BorderSide(color: Theme.of(context).focusColor)),
                          prefixIcon: Icon(
                            UiIcons.envelope,
                            color: Theme.of(context).focusColor.withOpacity(0.6),
                          ),
                        ),
                        controller: emailcontroller,
                       //onSaved: (value)=> emails= value,

                      ),
                      SizedBox(height: 20),
                      
                      new TextFormField(
                        style: TextStyle(color: Theme.of(context).focusColor),
                        keyboardType: TextInputType.text,
                        obscureText: !_showPassword,
                        decoration: new InputDecoration(
                          hintText: 'Password',
                          hintStyle: Theme.of(context).textTheme.body1.merge(
                                TextStyle(color: Theme.of(context).focusColor.withOpacity(0.6)),
                              ),
                          enabledBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Theme.of(context).focusColor.withOpacity(0.2))),
                          focusedBorder:
                              UnderlineInputBorder(borderSide: BorderSide(color: Theme.of(context).focusColor)),
                          prefixIcon: Icon(
                            UiIcons.padlock_1,
                            color: Theme.of(context).focusColor.withOpacity(0.6),
                          ),
                          suffixIcon: IconButton(
                            onPressed: () {
                              setState(() {
                                _showPassword = !_showPassword;
                              });
                            },
                            color: Theme.of(context).focusColor.withOpacity(0.4),
                            icon: Icon(_showPassword ? Icons.visibility_off : Icons.visibility),
                          ),
                        ),
                        controller: passwordcontroller,
                          onSaved: (value)=> passwords= value,

                      ),
                      SizedBox(height: 20),
                      FlatButton(
                        onPressed: () {},
                        child: Text(
                          'Forgot your password ?',
                          style: Theme.of(context).textTheme.body1,
                        ),
                      ),
                      SizedBox(height: 30),
                      FlatButton(
                        padding: EdgeInsets.symmetric(vertical: 12, horizontal: 70),
                        
                          // 2 number refer the index of Home page
                        
                          onPressed: ()  {
                         loginUser(emailcontroller.text,passwordcontroller.text,context)  ; 
                   // createnewuser(emailcontroller.text, passwordcontroller.text);
                  
                     },
                   
                        
                        child: Text(
                          'Login',
                          style: Theme.of(context).textTheme.title.merge(
                                TextStyle(color: Theme.of(context).primaryColor),
                              ),
                        ),
                        color: Theme.of(context).accentColor,
                        shape: StadiumBorder(),
                      ),
                      SizedBox(height: 50),
                      Text(
                        'Or using social media',
                        style: Theme.of(context).textTheme.body1,
                      ),
                      SizedBox(height: 20),
                      new SocialMediaWidget()
                    ],
                  ),
                ),
              ],
            ),
            FlatButton(
              onPressed: () {
                Navigator.of(context).pushNamed('/SignUp');
              },
              child: RichText(
                text: TextSpan(
                  style: Theme.of(context).textTheme.title.merge(
                        TextStyle(color: Theme.of(context).primaryColor),
                      ),
                  children: [
                    TextSpan(text: 'Don\'t have an account ?'),
                    TextSpan(text: ' Sign Up', style: TextStyle(fontWeight: FontWeight.w700)),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
Future<PostLogin> createPost(String url, {Map body}) async {
  return http.post(url, body: body).then((http.Response response) {
    final int statusCode = response.statusCode;
 
    if (statusCode < 200 || statusCode > 400 || json == null) {
      throw new Exception("Error while fetching data");
    }
    print(json.decode(response.body));
    return PostLogin.fromJson(json.decode(response.body));
  });
}
Future<void> fetchuserdetails(String _email,String _pass) async{
  const url = 'https://daangor.com/api/login';
   PostLogin newPost = new PostLogin(
                        email:  _email,
                        password : _pass);
                         final http.Response response = await http.post(url, 
                    body: json.encode(newPost.toMap()) );
//final response= await http.post('https://daangor.com/api/login'),
 // body: json.encode(newPost.toMap()) );
 if (response.statusCode == 200) {
    // If the server did return a 200 OK response,
    // then parse the JSON.
    Map<String, dynamic>  userdetail= json.decode(response.body);

    print(userdetail['name']);
  } else {
    // If the server did not return a 200 OK response,
    // then throw an exception.
  //  throw Exception('Failed to load album');
  }
}

Future<Userdetail> createuser( String email,String password,) async {
  final http.Response response = await http.post(
    'https://daangor.com/api/login',
    headers: <String, String>{
      'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
    },
    body: jsonEncode(<String, String>{
       'email': email,
      'password': password,
      
    }),
  );
  
    // If the server did return a 201 CREATED response,
    // then parse the JSON.
    Userdetail userdetail= Userdetail.fromJson(json.decode(response.body));
    print(userdetail.name);
    return userdetail;

}
loginUser(String email,String password,BuildContext context) async {

  Dio dio=new Dio();

  FormData formData = new FormData.fromMap({
    "email": email,
    "password": password,

  });
   
   Response response = await dio.post("https://daangor.com/api/login", 
    options: Options(
        followRedirects: false,
        validateStatus: (status) { return status < 500; }
    ),
    data: formData);
   print('response'+response.toString());
 
  var data= json.decode(response.data);
 

   print(''+data["user"]["name"]);

   Navigator.of(context).pushNamed('/Tabs', arguments: 2);
   //  Navigator.of(context).pushNamed('/Home');
   // response.data["data"]["token"];

//response.data["data"]["token"];
//{"login":"true","user":{"id":"10","name":"Ravi Soni","email":"dazzsoni@gmail.com",
//"address":null,"phone":null,"website":null,"social":"{\"facebook\":null,\"twitter\":null,\"linkedin\":null}"
//,"about":null,"password":"7c4a8d09ca3762af61e59520943dc26494f8941b","role_id":"2",
//"wishlists":"[]","verification_code":"5b08ae4fef453183cacb99121fa667b7","is_verified":"1",
//"token":"hPVAkgmdySeGRNxjl6Fn6YdyBwqiXZxpJFrzHiD8uEV9vIb5nYG7qS2wsv4h4J3lrasOCaoO8R95cz3C"}}


}
Future<dynamic> createnewuser(String email, String password) {
Map<String, dynamic>createdetils={
  'email': email,
  'password': password,
};
print(json.encode(createdetils)); 

http.post('https://daangor.com/api/login',
headers: <String, String>{
      'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
    },
    body: json.encode(createdetils)).then((http.Response response){
      Map<String , dynamic> responsedata = json.decode(response.body);
      print(responsedata);
      print('response body : ${response.body}');
      print(response.request);
    });
}
  //x-www-form-urlencoded