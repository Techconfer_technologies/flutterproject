class Slider {
  String image;
  Slider({this.image,});
}

class SliderList {
  List<Slider> _list;

  List<Slider> get list => _list;

  SliderList() {
    _list = [
      new Slider(image:'https://daangor.com/assets/frontend/images/home_section_1.jpg'),
    ];
  }
}
