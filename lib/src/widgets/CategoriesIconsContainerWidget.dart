import 'dart:convert';
import 'dart:ffi';

import 'package:daangor/src/models/category.dart';
import 'package:daangor/src/models/route_argument.dart';
import 'package:daangor/src/widgets/CategoryIconWidget.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class CategoriesIconsContainerWidget extends StatefulWidget {

  CategoriesIconsContainerWidget({
    Key key, 
    @required CategoriesList categoriesList,
    this.onPressed
    }) :super(key: key);

  final ValueChanged<String> onPressed;

  @override
  _CategoriesIconsContainertState createState() => _CategoriesIconsContainertState();
}
getCategoryList () async {
 Dio dio=new Dio();

   Response response = await dio.get("https://daangor.com/api/getallcategory");
   print('categoryList'+response.toString());
 
  var data= json.decode(response.data);
  print(data);
 }
class _CategoriesIconsContainertState extends State<CategoriesIconsContainerWidget> {
  CategoriesList categoriesList = new CategoriesList();
  @override
  Widget build(BuildContext context) {
    getCategoryList();
    return  Container(
        child: Wrap(
          alignment: WrapAlignment.spaceBetween,
          children:_buildSuggestions(categoriesList.list,context)
        ),
    );
  }
}
Future <void> fetch() async{
   const url = 'https://daangor.com/api/login';
    final response = await http.get(url);
}

  _buildSuggestions(List<Category> list,BuildContext context) {
    List<Widget> categories = List();
      list.forEach((item) {
      categories.add(
        Container(
          padding: EdgeInsets.only(bottom: 20),
          child: CategoryIconWidget(
            category: item,
             onPressed: (id) {
              // Navigator.of(context).pushNamed('/Categorie', arguments: new RouteArgument(id: item.id, argumentsList: [item]));
             },
          ),
        ),
      );
    }
    );return categories;
  }