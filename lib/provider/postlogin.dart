import 'package:flutter/material.dart';

class PostLogin{

  final String email;
  final String password;
   
    PostLogin({
       this.email,
        this.password,      
        });
          factory PostLogin.fromJson(Map<String, dynamic> json) {
    return PostLogin(
      email: json['email'],
      password: json['password'],
      );
  }
  Map toMap() {
    var map = new Map<String, dynamic>();
    map["email"] = email;
    map["password"] = password;
     return map;
  }
}

